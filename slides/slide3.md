---
tags: slide
layout: slide.njk
title: MiCM ResearchMatch program
image: slider_3.jpg
alt: MiCM ResearchMatch Slide
---

Are you a McGill data sciences researcher looking for interesting problems to
solve?

Are you a McGill life sciences researcher with interesting datasets requiring
innovative analysis approaches?

<a href="https://www.mcgill.ca/micm/micm-researchmatch">Learn More</a>
