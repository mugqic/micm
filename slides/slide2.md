---
tags: slide
layout: slide.njk
title: Data for Better Health
image: slider_2.jpg
alt: Data Slide
---

Our objective is to transform life science research and health care delivery
by better incorporating the use rich datasets and advanced computational
methods.
