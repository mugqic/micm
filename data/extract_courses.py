#!/usr/bin/env python3

import csv
import datetime
import json
import re
import requests
import sys


RE_COURSE_NAME = re.compile(r"^([A-Z]+[\s]*[0-9/D]+)?[:]?\s*(.+)$")
RE_CREDITS = re.compile(r"^([0-9]+)\s*(.+)$")

CALENDAR_TEMPLATE = "https://www.mcgill.ca/study/{}-{}/courses/{}-{}"


def usage():
    print("Usage: {} file_to_process.csv file_to_output.json".format(sys.argv[0]))
    exit(1)


def process_text(unp: str) -> str:
    return re.sub(r"\s+", r" ", unp.replace("\u00a0", " ").replace("\u2018", "'").replace("\u2019", "'").strip())


def main(args):
    file_path = args[0]
    file_out = args[1]
    with open(file_path, "r", newline="") as fi, open(file_out, "w") as fo:
        course_data = []

        next(fi)
        reader = csv.DictReader(fi)

        for row in reader:
            name = row["Course Name"].strip().replace("\xa0", " ")

            try:
                code, title = re.match(RE_COURSE_NAME, name).group(1, 2)
            except AttributeError:
                continue

            print(name)

            try:
                f_credits, format_type = re.match(RE_CREDITS, row["Format (coded)"]).group(1, 2)
                format_type = format_type.strip().lower()
            except AttributeError:
                f_credits, format_type = None, None

            instructors = row["Contact name"].strip().replace(") ", ");").replace(", ", ";").split(";")
            instructor_emails = row["Contact email"].lower().replace(" ", "").replace(",", ";").split(";")
            instructor_emails = [(e if e.lower() != "na" and e.strip() != "" else None) for e in instructor_emails]
            while len(instructor_emails) < len(instructors):
                instructor_emails.append(None)
            instructor_sites = row["website"].replace(", ", ";").split(";")
            instructor_sites = [(s if s.lower() != "na" and s.strip() != "" else None) for s in instructor_sites]
            while len(instructor_sites) < len(instructors):
                instructor_sites.append(None)

            course = {
                "code": code,
                "title": title,
                "offered_by": process_text(row["Department or group offering"]),
                "offered_recently": True,
                "target_audience": process_text(row["Target Audience"]),
                "target_audience_coded": process_text(row["target audience (coded)"]).lower(),
                "background": row["Assumed Technical Background relevant to the course (Limited, Moderate, "
                                  "Expert)"].strip(),
                "format": process_text(row["Format"]),
                "credits": f_credits,
                "format_type": format_type,
                "length": row["Length (coded)"].strip(),
                "general_description": process_text(row["General description of topics covered"]),
                "topics": row["Topics (coded)"].strip().lower(),
                "subtopics": row["Subtopics (coded)"].strip().lower(),
                "software": process_text(row["Software (coded)"]).lower(),
                "prerequisites": process_text(row["Prerequisites"]),
                "instructors": [{
                    "name": instructor[0],
                    "email": instructor[1],
                    "site": instructor[2]
                } for instructor in zip(instructors, instructor_emails, instructor_sites)],
                "open_to": process_text(row["Who is the course open to"]),
                "feedback": process_text(row["Feedback from students?"]),
                "contact_instructors": row["contact instructors to ask more details about who it is "
                                           "for/outline/student background"].strip(),
                "notes": row["Notes"].strip(),
                "contacted_professor": (row["contacted Professor (Y/N)"].strip() == "Y"
                                        if row["contacted Professor (Y/N)"].strip() != "" else None),
                "institution": "mcgill",  # For future use
                "offered": None  # To be fetched
            }

            if course["institution"] == "mcgill" and code is not None:
                code_prefix = code.replace(" ", "")[:4].lower()
                code_number = code.replace(" ", "")[4:]

                if "/" in code_number:
                    # Split course numbers. Just go with the first one for now...
                    # TODO: Maybe link to both?
                    code_number = code_number.split("/")[0]

                # March and before: show year-1 -> year
                # April and after: show year -> year + 1

                today = datetime.datetime.today()
                year_1 = today.year - (1 if today.month <= 3 else 0)
                year_2 = today.year + (0 if today.month <= 3 else 1)

                calendar_url = CALENDAR_TEMPLATE.format(year_1, year_2, code_prefix, code_number)

                r = requests.get(calendar_url)
                if r.status_code == 200 and "This course is not scheduled" not in r.text:
                    # TODO: Show on site if course is not scheduled

                    course["offered"] = {
                        "url": calendar_url,
                        "semesters": []
                    }

                    if "Fall {}".format(year_1) in r.text:
                        course["offered"]["semesters"].append("Fall {}".format(year_1))
                    if "Winter {}".format(year_2) in r.text:
                        course["offered"]["semesters"].append("Winter {}".format(year_2))
                    if "Summer {}".format(year_2) in r.text:
                        course["offered"]["semesters"].append("Summer {}".format(year_2))

                else:
                    past_2 = ((year_1 - 2, year_2 - 2), (year_1 - 1, year_2 - 1))
                    offered_recently = False

                    for y1, y2 in past_2:
                        calendar_url = CALENDAR_TEMPLATE.format(y1, y2, code_prefix, code_number)
                        r = requests.get(calendar_url)
                        if r.status_code == 200 and "This course is not scheduled" not in r.text:
                            offered_recently = True

                    course["offered_recently"] = offered_recently

            if course["offered_by"] == "" and course["target_audience"] == "":
                # Heuristic for "bad" row
                continue

            course_data.append(course)

        course_data = sorted(course_data, key=lambda c: (c["offered_by"], c["code"]))

        json.dump(course_data, fo, indent=2)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
    main(sys.argv[1:])
