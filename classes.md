---
tags: page 
layout: base.njk
title: Classes
url: /classes/
---

![Computational Medicine Courses page header](/img/courses_header.jpg)

# Computational Medicine Courses

<p style="font-family: 'CartoGothicStdBook', sans-serif; font-size: 1rem;">Welcome to the MiCM course page. Below is a list of courses related to
computational medicine.</p>

<!-- TODO: ADD LINK FOR SUBMISSION FORM -->

<div class="flex-row">
    <div class="flex-row__col submission-col">
        <p>Are you offering computational medicine courses or workshops? Let us know and we'll gladly add your course
        to our listing.</p>
        <a href="https://mcgill.ca/micm/course-submission">Submission Form</a>
    </div>
    <div class="flex-row__col contact-col">
        <p>Need to make corrections on the information to your listed course? Feel free to let us know.</p>
        <a href="https://mcgill.ca/micm/course-corrections">Correction Form</a>
    </div>
</div>

<div class="box" id="classes-filter">
    <h2>Filter Classes</h2>
    <div id="classes-search">
        <label for="filter-bar">Search Classes:</label>
        <input type="text" name="filter-bar" id="filter-bar" placeholder="e.g. 'statistics' or 'offered_by=experimental medicine'">
    </div>
    <div id="classes-suggestions">
        <h3>Suggested Filters</h3>
        <span>Keep typing...</span>
        <ul></ul>
    </div>
    <div id="classes-premade-filters"></div>
</div>
<div id="classes-list">
    <h2 id="course-header">Courses Available <a class="download-courses" href="/data/courses.csv">Download in CSV Format</a></h2>
</div>
