/*
 * .eleventy.js
 */

const pageOrdering = ["About", "Researchers", "Events", "Programs", "Job Opportunities", "Classes", "Contact"];

module.exports = function (eleventyConfig) {
    eleventyConfig.addCollection("page", collection => {
        return collection.getFilteredByTag("page").sort((a, b) => {
            return pageOrdering.indexOf(a.data.title) - pageOrdering.indexOf(b.data.title);
        });
    });

    eleventyConfig.addCollection("upcoming_events", collection => {
        const pastItems = collection.getFilteredByTag("past");

        return collection.getFilteredByTag("event").filter(e => !pastItems.includes(e)).sort((a, b) => {
            return 0; // TODO
            // return pageOrdering.indexOf(a.data.title) - pageOrdering.indexOf(b.data.title);
        });
    });

    eleventyConfig.addCollection("past_events", collection => {
        const pastItems = collection.getFilteredByTag("past");

        return collection.getFilteredByTag("event").filter(e => pastItems.includes(e)).sort((a, b) => {
            return 0; // TODO
            // return pageOrdering.indexOf(a.data.title) - pageOrdering.indexOf(b.data.title);
        });
    });

    return {
        templateFormats: [
            'md',
            'css', // css is not yet a valid template extension
            'png',
            'jpeg',
            'jpg',
            'gif',
            'svg',
            'json',
            'js',
            'eot',
            'woff',
            'ttf',
            'csv'
        ],
        passthroughFileCopy: true
    };
};
