---
tags:
- event
layout: event.njk
title: Fundamentals in Neuro Data Science
display_date: May 6 to 11 2019
---

Neuroscientists rely increasingly on data accessible online and on data science procedures for their investigations. Data science now offers a key set of tools and methods to efficiently analyse, visualize and interpret neuroscience data. Concurrently, there is a growing concern in the life sciences that many results produced are difficult or even impossible to reproduce. This is referred to as the reproducibility crisis, which concerns most of biomedical fields [F. Collin].

This first week of the computational neuroscience seminar series is bringing together software and analytical tools and methods. It will teach students how to best use the fundamentals of data science in their daily work to produce reproducible results. We will take examples in neuroimaging or imaging genetics, and see how to use computational tools, statistical and machine learning techniques, and collaborative and open science methodology to generate results that are statistically solid and computationally reproducible. While a large part of this course is language agnostic, we will teach and use python throughout the course. Students will start to work on projects that they will continue during the following computational neuroscience seminar weeks.

The course will require that you have some basic programming experience and some notion of statistical analysis, but will be aimed at life scientists (neurologists, psychiatrists, pyschologists, neuroscientists) who wish to improve their research practices, or students who want an introduction to data science with examples in neuroscience and neuroimaging. We will strive to avoid the classic problem described in Figure 1.

#### Date: May 6 to 11, 2019

Location: T.B.A

Schedule: T.B.A
 
#### Topics to be covered:

 * Introduction to the course
 * Epistemiology and lesson from the past
 * Computational Basics: shell and git
 * Computational Basics: python for programmers
 * Git distributed and collaboratif
 * Scientific python ecosystem
 * Containers: Docker and dockerhub
 * Exploring and visualizing data
 * Statistical tools - the basics
 * Model comparison, cross validation, Sampling techniques
 * Machine learning

More information coming soon.
