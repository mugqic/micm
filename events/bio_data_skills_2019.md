---
tags:
- event
layout: event.njk
title: Bioinformatics Data Skills 101
display_date: April 18, 2019
---

A series of four workshops for beginners who want to learn basic bioinformatics tools. For each workshop, attendees will receive, two weeks prior to the event, online material to read before the workshop. The in-class content will be focused on exercises and practical computing.

Une série de quatre ateliers de programmations sur les connaissances de bases en bio-informatique. Pour chaque atelier, les participants recevrons du matériel en ligne à lire avant l'atelier. Le contenu en classe sera principalement des exercices et exemples pratiques.

**Agenda:**

April 18 - 12:30pm-4:30pm - Command Line Interface (Bash) <br>
May 1 - 9:30am-1:30pm - R <br>
May 15 - 9:30am-1:30pm - Python <br>
June 10 - 1pm-5pm - Git <br>

**Location:**

RI Auditorium ES1.1129 <br>
RI-MUHC (Glen) <br>
1001 Decarie blvd <br>
Montreal, QC H4A 3J1 <br>

For Tickets: [Click here](https://www.eventbrite.ca/e/bioinformatics-data-skills-101-tickets-58105269425)

**Contact:**

If you have questions about the workshops, contact us at
**[bioinformatics.rimuhc@mcgill.ca](mailto:bioinformatics.rimuhc@mcgill.ca)**

Si vous avez des questions à propos des ateliers, contactez nous par courriel:
**[bioinformatics.rimuhc@mcgill.ca](mailto:bioinformatics.rimuhc@mcgill.ca)**
