# MiCM Website

## Dependencies

Compiling the website requires [Eleventy](https://www.11ty.io/), a static site
generator written in JavaScript. Changes in Markdown can be committed without
compiling, but the changes will not be visible in the generated HTML.

To install Eleventy, first make sure NodeJS and NPM are installed on your
development machine. Then, install Eleventy with the following command:

```bash
npm install -g eleventy
```

This command may need to be run with `sudo` privileges.


## Project Structure

TODO


## Editing the Website

Pages can be created and updated using Markdown (`.md`) files placed in the
root directory of the project.

Currently, the only **actually relevant** files to the non-WMS MiCM site are
`classes.md` and `profiles.md`. The other pages are just used to dynamically
create navigation links in the menu bar.

To edit the markup or static text of these pages, edit their corresponding
Markdown files.

### Updating Using Eleventy (Locally)

To "build" (update the actually displayed) site, run the following command
while in a Terminal window while in the `micm` site working directory:

```
eleventy
```

While making substantial changes, "watch" mode can be used to monitor for
updates and constantly re-build:

```
eleventy --watch
```

### Deploying Remotely

Deployment is controlled using the [MiCM CPanel](https://micm.mcgill.ca:2083/).

Once signed in, go to "Git Version Control", and click the "Manage" button for
the `micm` repository.

Then, go to the "Pull or Deploy" tab. Click **Update from Remote** first to
fetch the latest changes, and then deploy them by clicking
"Deploy HEAD Commit".

### Updating Classes

Class data is stored and managed through the `data/` directory in the
repository, and copied by Eleventy into the production site folder.

The class data is converted from a CSV file (see `data/courses.csv`) into a
JSON file (see `data/courses.json`). Columns must match exactly with what can
be seen in the current CSV file.

To update the JSON file with data from a new CSV file, perform the following
steps:

If not done already, create a Python 3 virtual environment in `data/` with
the following command:
     
```
virtualenv -p python3 ./env
```
  
Source the virtual environment and install / update dependencies with the
following commands:
     
```
source env/bin/activate
pip install -r requirements.txt
```
     
Run the following command to process the CSV, replacing the CSV filename
with whatever corresponds to the new CSV file:
     
```
python ./extract_courses.py courses.csv courses.json
```
     
Exit the data directory and update the site using Eleventy:
  
```
cd ..
eleventy
```

### Profiles from McGill Network

Profiles are displayed on the MiCM website via an embedded version of
[McGill Network](http://mcgill-network.vhost38.genap.ca/), and are pulled from
McGill's [UNIWeb](https://uniweb.mcgill.ca/) research network.


## Development NGINX Configuration

The following configuration can be used, with the static files path specified,
to serve the site locally from NGINX using a `.local` domain name (entered into
`/etc/hosts`):

```nginx
server {
    listen 80;

    root /path/to/micm/_site;
    index index.html index.htm;

    server_name micm.local;

    location / {
        try_files $uri $uri/ =404;
    }
}
```
