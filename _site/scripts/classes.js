let allClasses;
let searchClasses;
let searchTerm = "";
let suggestedFilters = new Set();

const fields = ["code", "title", "offered_by", "target_audience", "background", "format", "credits", "format_type",
    "length", "general_description", "topics", "subtopics", "software", "prerequisites", "contact_info", "name",
    "email", "open_to", "feedback", "contact_instructors", "notes", "contacted_professor"];

const shortValueFields = ["code", "title", "offered_by", "credits", "format_type", "topics", "subtopics", "software",
    "open_to"];

const updateClasses = () => {
    updatePreMadeFilter("topics", true);
    updatePreMadeFilter("length", true);
    updatePreMadeFilter("target_audience_coded", true);
    updatePreMadeFilter("background", true);

    const classes = d3.select("#classes-list")
        .selectAll(".class")
        .data(searchClasses, c => c["code"]);

    const newClass = classes.enter()
        .append("div")
        .classed("class", true);

    const classHeader = newClass.append("h3");
    classHeader.append("span")
        .classed("class__code", true)
        .classed("hidden", c => !c["code"])
        .text(c => c["code"]);

    classHeader.append("span")
        .classed("class__title", true)
        .text(c => c["title"]);

    classHeader.append("span")
        .classed("class__credits", true)
        .classed("hidden", c => !c["credits"])
        .text(c => `(${c["credits"]} credits)`);

    classHeader.append("div")
        .classed("class__status", true)
        .classed("hidden", c => c["offered_recently"])
        .html(c => c["offered_recently"] ? "" : "<i class=\"material-icons\">error_outline</i> " +
            "This course has not been offered in the past 3 years.");

    classHeader.selectAll("button.mcgill-course-calendar")
        .data(c => c["offered"] !== null ? [c["offered"]] : [])
        .enter()
        .append("div")
        .classed("class__calendar_buttons", true)
        .selectAll("a")
        .data(d => d["semesters"].map(s => ({d, s})))
        .enter()
        .append("a")
        .attr("href", d => d.d["url"])
        .attr("target", "_blank")
        .attr("rel", "nofollow noreferrer")
        .text(d => `e-Calendar (${d.s})`);

    newClass.append("div").classed("class__instructors", true)
        .html(c =>
            "<strong>Instructor(s):</strong> " +
            c["instructors"].map(i =>
                i["site"]
                    ? `<a href="${i["site"]}" target="_blank" rel="nofollow noreferrer">${i["name"]}</a>`
                    : `${i["name"]}`).join(", "));

    const classOutline = newClass.append("div")
        .classed("class__outline", true);
    classOutline .append("h4").text("Outline");
    classOutline.append("p").html(c => c["general_description"].replace("\n", "<br>"));

    newClass.append("p").attr("class", "class__info")
        .html(c => `<strong>Offered By:</strong> ${c["offered_by"]} `);

    newClass.append("p").attr("class", "class__info")
        .html(c => `<strong>Duration:</strong> ${c["length"]} `);

    newClass.append("p").attr("class", "class__info")
        .html(c => `<strong>Target Audience:</strong> ${c["target_audience"].replace("\n", "<br>")}`);

    newClass.append("p").attr("class", "class__info")
        .html(c => `<strong>Topic(s):</strong> ${c["topics"]}`);

    newClass.append("p").attr("class", "class__info")
        .html(c => `<strong>Prerequisites:</strong> ${c["prerequisites"]}`);

    newClass.append("p").attr("class", "class__info")
        .html(c => `<strong>Assumed Background:</strong> ${c["background"]}`);

    newClass.append("p").attr("class", "class__info")
        .html(c => `<strong>Open To:</strong> ${c["open_to"]}`);

    classes.exit().remove();
};

const refreshSearch = () => {
    const codedSearch = searchTerm.split("=").length === 2
        && fields.includes(searchTerm.split("=")[0].toLocaleLowerCase());

    const filterTopics = getPreMadeFilterValues("topics");
    const filterFormat = getPreMadeFilterValues("length");
    const filterTargetAud = getPreMadeFilterValues("target_audience_coded");
    const filterBackground = getPreMadeFilterValues("background");

    if (!codedSearch) {
        searchClasses = [];

        allClasses.forEach(c => {
            let found = false;
            fields.forEach(f => {
                if (!c[f]) return;
                let classTerm = c[f].toString().toLocaleLowerCase();
                if (!classTerm.includes(searchTerm)) return;

                found = true;
            });

            const ft = filterTopics.length === 0 || filterTopics.includes(c["topics"]);
            const ff = filterFormat.length === 0 || filterFormat.includes(c["length"]);
            const fa = filterTargetAud.length === 0 || filterTargetAud.includes(c["target_audience_coded"]);
            const fb = filterBackground.length === 0 || filterBackground.includes(c["background"]);

            if (found && ft && ff && fa && fb) {
                searchClasses.push(c);
            }
        });

        updateClasses();
        return;
    }

    // Otherwise, need to handle field lookups

    const field = searchTerm.split("=")[0].toLocaleLowerCase();
    const term = searchTerm.split("=")[1].toLocaleLowerCase();

    searchClasses = [];

    allClasses.forEach(c => {
        if (!c[field]) return;
        let classTerm = c[field].toString().toLocaleLowerCase();
        if (!classTerm.includes(term)) return;

        const ft = filterTopics.length === 0 || filterTopics.includes(c["topics"]);
        const ff = filterFormat.length === 0 || filterFormat.includes(c["length"]);
        const fa = filterTargetAud.length === 0 || filterTargetAud.includes(c["target_audience_coded"]);
        const fb = filterBackground.length === 0 || filterBackground.includes(c["background"]);

        if (ft && ff && fa && fb) {
            searchClasses.push(c);
        }
    });

    // TODO
    updateClasses();
};


const buildPreMadeFilter = (filterNav, name, key, multiple) => {
    const filterContainer = filterNav.append("div")
        .attr("class", "filter-container")
        .attr("id", `filter-container-${key}`);

    const filterDrop = filterContainer.append("div")
        .attr("class", "filter-dropdown")
        .attr("id", `filter-dropdown-${key}`);

    filterDrop.on("click", () => {
        d3.event.stopPropagation();
        const oldStatus = d3.select(`#filter-container-${key}`).classed("shown");
        d3.selectAll(".filter-container").classed("shown", false);
        d3.select(`#filter-container-${key}`).classed("shown", !oldStatus);
    });
    filterDrop.append("span").classed("dropdown-label", true).text(name);
    filterDrop.append("div").classed("dropdown-active-filters", true);
    filterDrop.append("span").classed("material-icons", true).text("expand_more");

    const filter = filterContainer.append("div")
        .attr("class", "filter")
        .attr("id", `filter-${key}`)
        .on("click", () => { d3.event.stopPropagation(); });

    filter.append("h3").text(name);
    filter.append("div")
        .attr("class", "filter-inner")
        .attr("id", `filter-inner-${key}`);

    document.addEventListener("click", () => {
        d3.selectAll(".filter-container").classed("shown", false);
    });

    updatePreMadeFilter(name, key, multiple);
};


const updatePreMadeFilter = (key, multiple) => {
    // TODO: Base not on searchClasses but on filter-bar search classes
    if (searchClasses === undefined) return;
    const innerFilter = d3.select(`#filter-inner-${key}`);
    const values = [...new Set(allClasses
        .map(c => c[key])
        .filter(c => c !== null && c !== undefined && (c.toString() !== "")))
    ].sort();
    const counts = {};

    values.forEach(v => {
        counts[v] = allClasses.filter(c => c[key] === v).length;
    });

    const d3data = values.map(v => ({v: v, c: counts[v]}));

    const labels = innerFilter.selectAll("label").data(d3data, v => {
        return `${v.v}${v.c}`;
    });
    labels.exit().remove();

    const label = labels.enter().append("label").attr("data-key", v => `${v.v}${v.c}`);

    label.on("click", () => refreshSearch());

    label.append("input")
        .attr("type", multiple ? "checkbox" : "radio")
        .attr("name", key)
        .attr("value", v => v.v);
    label.append("span").text(v => `${v.v} (${v.c})`);
};


const getPreMadeFilterValues = key => {
    const vs = d3.selectAll(`input[name="${key}"]`).nodes()
        .filter(n => n.checked)
        .map(n => n.value);

    d3.select(`#filter-dropdown-${key}`).select("div.dropdown-active-filters")
        .text(vs.length === 0 ? "" : vs.length.toString())
        .classed("visible", vs.length > 0);

    return vs;
};


const updateSearchValue = () => {
    searchTerm = d3.select("#filter-bar").property("value").toLocaleLowerCase();
    if (searchTerm.length <= 3) {
        d3.select("#classes-suggestions").classed("show", true);
        d3.select("#classes-suggestions").select("ul").html("");
        d3.select("#classes-suggestions").select("span").attr("style", "display: inline;");
        return;
    }

    d3.select("#classes-suggestions").select("span").attr("style", "display: none;");

    suggestedFilters = new Set();

    allClasses.forEach(c => {
        let found = false;
        shortValueFields.forEach(f => {
            if (!c[f]) return;
            if (["topics", "length", "open_to"].includes(f)) return; // exclude hardcoded filters in suggestions
            let classTerm = c[f].toString().toLocaleLowerCase();
            if (!classTerm.includes(searchTerm)) return;

            found = true;

            suggestedFilters.add(`${f}=${classTerm}`);
        });
    });

    let filters = d3.select("#classes-suggestions").select("ul").selectAll("li")
        .data([...suggestedFilters], f => f);

    filters.enter().append("li").text(f => f).on("click", f => {
        d3.select("#filter-bar").property("value", f);
        searchTerm = f;
        d3.select("#classes-suggestions").classed("show", false);
        refreshSearch();
    });
    filters.exit().remove();

    if (suggestedFilters.size === 0) {
        d3.select("#classes-suggestions").classed("show", false);
    }
};


const buildFilter = data => {
    if (document.getElementById("classes-list") === null) return;

    allClasses = data;
    searchClasses = data;


    // Events

    d3.select("#filter-bar").on("focus", () => {
        d3.select("#classes-suggestions").classed("show", true);
        updateSearchValue();
        refreshSearch();
    });

    d3.select("#filter-bar").on("focusout", () => {
        // d3.select("#classes-suggestions").classed("show", false);
        d3.select("#classes-suggestions")
            .classed("show", searchTerm.length > 3 && suggestedFilters.size !== 0);

        refreshSearch();
    });

    d3.select("#filter-bar").on("keyup", () => {
        updateSearchValue();
    });

    d3.select("#filter-bar").on("change", () => {
        updateSearchValue();
        refreshSearch()
    });

    refreshSearch();

    const preMadeFilters = d3.select("#classes-premade-filters");

    const filterNav = preMadeFilters.append("nav");

    // Set up pre-defined filters.
    buildPreMadeFilter(filterNav, "Topics", "topics", true);
    buildPreMadeFilter(filterNav, "Format", "length", true);
    buildPreMadeFilter(filterNav, "Target Audience", "target_audience_coded", true);
    buildPreMadeFilter(filterNav, "Background", "background", true);

    updateClasses();
};

document.addEventListener("DOMContentLoaded", () => {
    d3.json("/data/courses.json").then(buildFilter);
});
